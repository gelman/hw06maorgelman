#include <iostream>
#define INVALID_VALUE 8200

void checkValue(int value, bool& error)
{
	if (value == INVALID_VALUE && !error)
	{
		std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << std::endl;
		error = true;
	}
}


int add(int a, int b, bool& error) {
	checkValue(a + b, error);
	return a + b;
}

int  multiply(int a, int b, bool& error) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a, error);
		checkValue(sum, error);
	};
	return sum;
}

int  pow(int a, int b, bool& error) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a, error);
		checkValue(exponent, error);
	};
	checkValue(exponent, error);
	return exponent;
}

int main(void) {

	bool error = false;
	int result = 0;
	
	/*valid*/
	result = add(90, 5, error);
	if (!error)
	{
		std::cout << result << std::endl;
	}

	/*invalid*/
	result = add(8200, 0, error); /*the function returns 8200*/
	if (!error)
	{
		std::cout << result << std::endl;
	}

	std::cout << std::endl;
	///////////////////////////////

	error = false;

	/*valid*/
	result = multiply(90, 5, error);
	if (!error)
	{
		std::cout << result << std::endl;
	}

	/*invalid*/
	result = multiply(4, 4100, error); /*the variable contains 8200 in the middle of the progress*/
	if (!error)
	{
		std::cout << result << std::endl;
	}

	std::cout << std::endl;
	//////////////////////////////////

	error = false;

	/*valid*/
	result = pow(9, 5, error);
	if (!error)
	{
		std::cout << result << std::endl;
	}

	/*invalid*/
	result = pow(8200, 1, error); /*the function returns 8200*/
	if (!error)
	{
		std::cout << result << std::endl;
	}

	system("pause");

	return 0;
}