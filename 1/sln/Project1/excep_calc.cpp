#include <iostream>
#include <string>
#define INVALID_VALUE 8200

void checkValue(int value)
{
	if (value == INVALID_VALUE)
	{
		throw(std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year."));
	}
}


int add(int a, int b) {

	checkValue(a + b);
	return a + b;
}

int  multiply(int a, int b) {
	int sum = 0;
	for (int i = 0; i < b; i++) {
		sum = add(sum, a);
		checkValue(sum);
	};
	return sum;
}

int  pow(int a, int b) {
	int exponent = 1;
	for (int i = 0; i < b; i++) {
		exponent = multiply(exponent, a);
		checkValue(exponent);
	};
	checkValue(exponent);
	return exponent;
}

int main(void) {

	try
	{
		/*valid*/
		std::cout << add(4, 2) << std::endl;
	}
	catch (const std::string& e)
	{
		std::cout << e;
	}

	try
	{
		/*invalid*/
		std::cout << add(8000, 200) << std::endl; /*the function returns 8200*/
	}
	catch (const std::string& e)
	{
		std::cout << e;
	}


	std::cout << std::endl;
	////////////////////////////

	try
	{
		/*valid*/
		std::cout << multiply(4, 2) << std::endl;
	}
	catch (const std::string& e)
	{
		std::cout << e;
	}

	try
	{
		/*invalid*/
		std::cout << multiply(2, 10000) << std::endl; /*in the progress 8200 is the value of some variable*/
	}
	catch (const std::string& e)
	{
		std::cout << e;
	}

	std::cout << std::endl;
	//////////////////////////

	try
	{
		/*valid*/
		std::cout << pow(4, 2) << std::endl;
	}
	catch (const std::string& e)
	{
		std::cout << e;
	}

	try
	{
		/*invalid*/
		std::cout << pow(8200, 1) << std::endl; /*the function returns 8200*/
	}
	catch (const std::string& e)
	{
		std::cout << e;
	}

	std::cout << std::endl;
	system("pause");
	return 0;
}