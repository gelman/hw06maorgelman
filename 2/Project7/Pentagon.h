#pragma once
#include "Shape.h"
#include "ShapeException.h"
#include "MathUtils.h"
#include <iostream>
#include <string>

class Pentagon : public Shape
{
public:
	Pentagon(std::string, std::string, double);
	~Pentagon();
	void draw();
	double CalArea();
	void setSide(double side);

private:
	double _side;
};

