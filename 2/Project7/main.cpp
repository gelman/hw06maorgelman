#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "InputException.h"
#include "Pentagon.h"
#include "Hexagon.h"

#define MAX_LEN 1
#define FIRST_LETTER 0

void checkType(std::string s)
{
	if (s.length() > MAX_LEN)
	{
		throw (std::string("Warning - Don't try to build more than one shape at once"));
	}
}


void checkInput(double in)
{
	if (std::cin.fail())
	{
		throw new InputException();
	}
}

int main()
{
	int numOfLetters = 0;
	std::string checkStr = "";

	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0,side = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Pentagon pen(nam, col, side);
	Hexagon hex(nam, col, side);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape *ptrpen = &pen;
	Shape *ptrhex = &hex;

	std::cout << "Enter information for your objects" << std::endl;
	const char circle = 'c', quadrilateral = 'q', rectangle = 'r', parallelogram = 'p'; char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, t - pentagon, h - hexagon" << std::endl;
		std::cin >> checkStr;
		try
		{
			checkType(checkStr);
			shapetype = checkStr[FIRST_LETTER];

			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				checkInput(rad);
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				checkInput(height);
				checkInput(width);
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				checkInput(height);
				checkInput(width);
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				checkInput(height);
				checkInput(width);
				checkInput(ang);
				checkInput(ang2);
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 't':
				std::cout << "enter name, color, and the pentagon's side" << std::endl;
				std::cin >> col >> nam >> side;
				checkInput(side);
				pen.setColor(col);
				pen.setName(nam);
				pen.setSide(side);
				ptrpen->draw();
				break;
			case 'h':
				std::cout << "enter name, color, and the hexagon's side" << std::endl;
				std::cin >> col >> nam >> side;
				checkInput(side);
				hex.setColor(col);
				hex.setName(nam);
				hex.setSide(side);
				ptrhex->draw();
				break;
			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (InputException* e)
		{
			printf(e->what());
			std::cin.clear();
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}
		catch (std::exception* e)
		{
			printf(e->what());
		}
		catch (std::string e)
		{
			std::cout << e << std::endl;
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}

	system("pause");
	return 0;

}