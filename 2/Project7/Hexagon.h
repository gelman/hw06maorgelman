#pragma once
#include "Shape.h"
#include "ShapeException.h"
#include "MathUtils.h"
#include <iostream>
#include <string>

class Hexagon : public Shape
{
public:
	Hexagon(std::string, std::string, double);
	~Hexagon();
	void draw();
	double CalArea();
	void setSide(double side);

private:
	double _side;
};
