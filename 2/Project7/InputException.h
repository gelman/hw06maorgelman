#pragma once
#include <exception>

class InputException : public std::exception
{
public:
	virtual const char* what() const
	{
		return "InputException: you can't put char or a string instead of number\n";
	}
};