#pragma once

#define PENTAGON_AREA_CONST 1.72048
#define HEXAGON_AREA_CONST 2.59808


class MathUtils
{
public:
	static double CalPentagonArea(double side)
	{
		return side * side *  PENTAGON_AREA_CONST;
	}
	static double CalHexagonArea(double side)
	{
		return side * side *  HEXAGON_AREA_CONST;
	}
};

