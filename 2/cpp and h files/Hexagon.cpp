#include "Hexagon.h"

void Hexagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "Side is " << this->_side << std::endl << "Area: " << CalArea() << std::endl;
}


Hexagon::Hexagon(std::string nam, std::string col, double side) :Shape(col,nam) {
	if (side < 0)
	{
		throw new shapeException();
	}
	else
	{
		this->_side = side;
	}
}

Hexagon::~Hexagon()
{
}

void Hexagon::setSide(double side)
{
	if (side < 0)
	{
		throw new shapeException();
	}
	else
	{
		this->_side = side;
	}
}

double Hexagon::CalArea()
{
	return MathUtils::CalHexagonArea(this->_side);
}