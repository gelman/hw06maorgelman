#include "Pentagon.h"

void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "Side is " << this->_side << std::endl << "Area: " << CalArea() << std::endl;
}


Pentagon::Pentagon(std::string nam, std::string col, double side):Shape(col,nam) {
	if (side < 0)
	{
		throw new shapeException();
	}
	else
	{
		this->_side = side;
	}
}

Pentagon::~Pentagon()
{
}

void Pentagon::setSide(double side)
{
	if (side < 0)
	{
		throw new shapeException();
	}
	else
	{
		this->_side = side;
	}
}

double Pentagon::CalArea()
{
	return MathUtils::CalPentagonArea(this->_side);
}